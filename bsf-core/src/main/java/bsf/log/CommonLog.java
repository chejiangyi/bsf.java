package bsf.log;

import bsf.base.BsfConfig;
import bsf.log.base.BaseLog;
import bsf.log.base.CommonLogConfig;
import bsf.util.DateUtil;
import bsf.util.FileUtil;

import java.util.Date;

/**
 * Created by chejiangyi on 2017/3/28.
 */
public class CommonLog extends BaseLog {
    public static void write(String msg, Class cls)
    {
        if (!CommonLogConfig.getConfig().IsWriteLog)
            return;
        if (CommonLogConfig.getConfig().LocalFile)
        {
            String message = "***【CommLog】【"+ DateUtil.toString(new Date(),"yyyy-MM-dd HH:mm:ss:SSS")+"】***"+ FileUtil.lineSeparator()+
                    getTypeName(cls)+msg + FileUtil.lineSeparator()+FileUtil.lineSeparator();
            String filepath = FileUtil.getFilePath(FileUtil.getDirectoryPath(CommonLog.class), "commonlog", DateUtil.toString(new Date(),"yyyy-MM-dd") + ".common.log");
            FileUtil.createDirectory(filepath);
            FileUtil.appendAllText(filepath,message);
        }
        //// TODO: 2017/3/31 接入监控平台未实现
    }
}
