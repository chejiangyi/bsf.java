package bsf.log.base;

import bsf.base.BsfConfig;
import bsf.system.configparser.ConfigStringParser;
import bsf.system.configparser.ConfigStringParserManager;

/**
 * Created by chejiangyi on 2017/3/31.
 */
public class DebugLogConfig extends BaseLogConfig {
    /**
     * 是否写入内存
     */
    public boolean Memory = true;

    /**
     * 是否写入本地文件
     */
    public boolean LocalFile=true;

   public static DebugLogConfig getConfig()
   {
       return new ConfigStringParserManager().GetParser(DebugLogConfig.class).Parse(BsfConfig.getDebugLog(),DebugLogConfig.class);
   }

}
