package bsf.log;

import bsf.base.BsfConfig;
import bsf.log.base.ErrorLogConfig;
import bsf.mvc.HttpContext;
import bsf.util.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.Date;
import java.util.Map;

/**
 * Created by chejiangyi on 2017/3/28.
 */
public class ErrorLog {

    public static void write(String errormsg,Throwable exception,Class cls)
    {
        if (exception == null)
        {
            exception = new RuntimeException();
        }
        //增加参数错误记录
        String requestparamsinfo = "";
        if (HttpContext.getRequest()!= null)
        {
            try
            {
                for (Map.Entry<String,String> param: HttpContext.getParameterMap().entrySet())
                {
                    if (param.getValue()!= null)
                    {
                        requestparamsinfo += String.format("[%1$s:%2$s]", param.getKey(), param.getValue());
                    }
                }
            }
            catch (java.lang.Exception e)
            {
            }
        }

        String message =  "***【ErrorLog】【"+ DateUtil.toString(new Date(),"yyyy-MM-dd HH:mm:ss:SSS")+"】***"+ FileUtil.lineSeparator()
                + "【时间】" + DateUtil.toString(new Date(),"yyyy-MM-dd HH:mm:ss:SSS") + FileUtil.lineSeparator()
                + "【服务器】" + NetworkUtil.getLocalIp() + FileUtil.lineSeparator()
//                + "【数据库】" + ServerIp + IOUtil.lineSeparator()
                + "【补充信息】" + StringUtil.nullToEmpty(errormsg) + FileUtil.lineSeparator()
                + "【原始信息】"  + StringUtil.nullToEmpty( exception.getMessage()) + FileUtil.lineSeparator()
                + "【request参数】" + requestparamsinfo + FileUtil.lineSeparator()
                + "【堆栈打印】" + ((StringUtils.startsWithIgnoreCase(cls.getPackage().getName(),"bsf")==true&&ErrorLogConfig.getConfig().IsPrintBsfStackTrace==false)?"bsf stackstarce closed":ExceptionUtil.getFullStackTrace(exception)) + FileUtil.lineSeparator()
                + "【详细错误】" + ExceptionUtil.getDetailMessage(exception)+ FileUtil.lineSeparator();
        System.err.println(message);
        if (!ErrorLogConfig.getConfig().IsWriteLog)
        {
            return;
        }
        if (ErrorLogConfig.getConfig().LocalFile)
        {
            String filepath = FileUtil.getFilePath(FileUtil.getDirectoryPath(ErrorLog.class), "errorlog", DateUtil.toString(new Date(),"yyyy-MM-dd") + ".error.log");
            FileUtil.createDirectory(filepath);
            FileUtil.appendAllText(filepath,message);
        }
        if (ErrorLogConfig.getConfig().MonitorPlatform)
        {
//            message = "【补充信息】" + errormsnginfo.msg + "\r\n" + "【原始信息】" + exp.Message;
//
//            String remark = "【服务器】" + CurrentIp + "\r\n" + "【数据库】" + ServerIp + "\r\n" + "【request参数】" + requestparamsinfo + "\r\n" + "【错误备注】" + errormsnginfo.remark.NullToEmpty();
//            BaseService.Monitor.Base.Entity.ErrorLogInfo tempVar = new BaseService.Monitor.Base.Entity.ErrorLogInfo();
//            tempVar.logcreatetime = new java.util.Date();
//            tempVar.developer = (String.IsNullOrWhiteSpace(errormsnginfo.developer.NullToEmpty()) ? BSFConfig.ProjectDeveloper.NullToEmpty() : errormsnginfo.developer.NullToEmpty()).SubString2(50).NullToEmpty();
//            tempVar.logtag = errormsnginfo.logtag.SubString2(90).NullToEmpty();
//            tempVar.msg = message.SubString2(3800).NullToEmpty();
//            tempVar.logtype = (byte)errormsnginfo.logtype;
//            tempVar.projectname = BSFConfig.ProjectName.NullToEmpty();
//            tempVar.tracestack = exp.StackTrace.SubString2(2000).NullToEmpty();
//            tempVar.remark = remark.NullToEmpty();
//            BSF.BaseService.BaseServiceContext.MonitorProvider.AddErrorLog(tempVar);
        }
    }
}
