package bsf.log;

import bsf.base.BsfConfig;
import bsf.log.base.BaseLog;
import bsf.log.base.DebugLogConfig;
import bsf.util.DateUtil;
import bsf.util.FileUtil;

import java.util.Date;

/**
 * Created by chejiangyi on 2017/3/28.
 */
public class DebugLog extends BaseLog {
    public static void write(String msg, Class cls)
    {
        if (!DebugLogConfig.getConfig().IsWriteLog)
            return;
        if(DebugLogConfig.getConfig().Memory)
            System.out.println("【Debug】"+msg);
        if (DebugLogConfig.getConfig().LocalFile)
        {
            String message =  "***【DebugLog】【"+ DateUtil.toString(new Date(),"yyyy-MM-dd HH:mm:ss:SSS")+"】***"+ FileUtil.lineSeparator()+ getTypeName(cls) + msg + FileUtil.lineSeparator()+FileUtil.lineSeparator();
            String filepath = FileUtil.getFilePath(FileUtil.getDirectoryPath(DebugLog.class), "debuglog", DateUtil.toString(new Date(),"yyyy-MM-dd") + ".debug.log");
            FileUtil.createDirectory(filepath);
            FileUtil.appendAllText(filepath,message);
        }
    }
}
