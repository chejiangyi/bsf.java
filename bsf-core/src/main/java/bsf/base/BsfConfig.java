package bsf.base;

import bsf.baseservice.BaseServiceContext;
import bsf.util.FileUtil;
import bsf.util.IOUtil;
import bsf.util.PropertiesUtil;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by chejiangyi on 2017/3/28.
 */
public class BsfConfig {
    /**
     * 默认Http连接池配置（全局）
     */
    public static String getDefaultHttpPoolConnectString() {
        return getValue("DefaultHttpPoolConnectString",
                "DefaultMaxPerRoute[=]500[;]DefaultMaxTotal[=]500[;]MaxPendingConnect[=]50[;]ConnectionRequestTimeout[=]5000[;]SocketTimeout[=]10000[;]ConnectTimeout[=]10000[;]RedirectsEnabled[=]false[;]");
    }
    /**
     * Debug日志配置
     */
    public static String getDebugLog()
    {
        return getValue("DebugLog", "IsWriteLog[=]true[;]Memory[=]true[;]LocalFile[=]true[;]");
    }
    /**
     * Error日志配置
     */
    public static String getErrorLog()
    {
        return getValue("ErrorLog", "IsWriteLog[=]true[;]LocalFile[=]true[;]MonitorPlatform[=]false[;]");
    }
    /**
     * Comm日志配置 普通日志
     */
    public static String getCommonLog()
    {
        return getValue("CommonLog", "IsWriteLog[=]true[;]LocalFile[=]true[;]MonitorPlatform[=]false[;]");
    }
    /**
     * 耗时日志配置 普通日志
     */
    public static String getTimeWatchLog()
    {
        return getValue("TimeWatchLog", "IsWriteLog[=]true[;]LocalFile[=]true[;]MonitorPlatform[=]false[;]");
    }

    /**
     * 耗时监控数据库连接
     */
    public static String getTimeWatchConnectionString() {
        return getValue("TimeWatchConnectionString", "");
    }

    /**
     * 监控平台数据库连接
     */
    public static String getMonitorPlatformConnectionString() {
        return StringUtils.isEmpty(monitorPlatformConnectionString) ? getValue("MonitorPlatformConnectionString", "") : monitorPlatformConnectionString;
    }

    public static void setMonitorPlatformConnectionString(String value) {
        monitorPlatformConnectionString = value;
    }
    private static String monitorPlatformConnectionString;

    /**
     * 统一配置中心数据库连接
     */
    public static String ConfigManagerConnectString = getValue("ConfigManagerConnectString", ""); //server=192.168.17.200;Initial Catalog=dyd_new_main;User ID=sa;Password=Xx~!@#;

    /**
     * 当前项目名称
     */
    public static String ProjectName = getValue("ProjectName", "未命名项目");

    /**
     * 当前项目默认开发人员
     */
    public static String getProjectDeveloper() {
        return getValue("ProjectDeveloper", "");
    }


    /**
     * 集群性能监控库连接
     */
    public static String getClusterConnectString() {
        return getValue("ClusterConnectString", "");
    }

    /**
     * 集群性能监控库连接
     */
    public static String getPlatformManageConnectString() {
        return getValue("PlatformManageConnectString", "");
    }

    /**
     * 耗时库连接
     */
    public static String getTimeWatchConnectString() {
        return getValue("TimeWatchConnectString", "");
    }

    /**
     * 集群性能监控库连接
     */
    public static String getUnityLogConnectString() {
        return getValue("UnityLogConnectString", "");
    }

    /**
     * 业务消息推送平台连接
     */
    public static String getNotifyPushConnectString() {
        return getValue("NotifyPushConnectString", "");
    }

    /**
     * 文件存储系统连接
     */
    public static String getFileStorageConnectString() {
        return getValue("FileStorageConnectString", "");
    }

    /**
     * TcpServer平台连接
     */
    public static String getTcpServerConnectString() {
        return getValue("TcpServerConnectString", "");
    }

    public final static String BsfPropertiesFileName = "bsf.properties";

    public static String getValue(String key, String defaultvalue) {
       String  bsfPropertiesFilePath = PropertiesUtil.tryfindPropertiesPath(BsfConfig.BsfPropertiesFileName, BsfConfig.class);
        if (bsfPropertiesFilePath!=null) {
            String value = PropertiesUtil.read(bsfPropertiesFilePath, key);
            if (value != null)
                return value;
        } else {
                if (BaseServiceContext.ConfigManagerProvider != null)
                {
                    RefObject<String> refObject = new RefObject(null);
                    if (BaseServiceContext.ConfigManagerProvider.tryGet(key,String.class, refObject) == true)
                    {
                        return refObject.Arg;
                    }
                }
        }


        return defaultvalue;
    }
}
