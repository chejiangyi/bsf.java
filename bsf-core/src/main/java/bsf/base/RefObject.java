package bsf.base;

/**
 * Created by chejiangyi on 2017/4/5.
 * java 没有ref和out语法(c语言和c#语言中ref和out语法替代实现)
 */
public class RefObject<T>
{
    public T Arg;
    public RefObject(T refarg)
    {
        Arg = refarg;
    }
}