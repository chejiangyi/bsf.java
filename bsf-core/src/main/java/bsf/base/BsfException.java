package bsf.base;

import bsf.util.StringUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chejiangyi on 2017/3/28.
 */
public class BsfException extends RuntimeException {
    public String BSFMessage="";
    public List<Throwable> BSFExceptionList = new ArrayList<Throwable>();

    public BsfException(String message)
    {
        this(message,null,null);
    }

    public BsfException(String message, Class cls)
    {
        this(message,null,cls);
    }

    public BsfException(String message, Throwable exp)
    {
        this(message,exp,null);
    }

    public BsfException(String message, Throwable exp, Class cls)
    {
        super(message,exp);
        if(exp!=null) {
            BSFMessage += getTypeName(cls) + StringUtil.nullToEmpty(message)+ StringUtil.nullToEmpty(exp.getMessage());
            BSFExceptionList.add(exp);
        }
        else
        {
            BSFMessage += getTypeName(cls)+ StringUtil.nullToEmpty(message);
        }
    }

    protected String getTypeName(Class cls)
    {
        return cls == null ? "" : "【" + cls.getName() + "】";
    }
}
