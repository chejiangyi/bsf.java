package bsf.base;

/**
 * Created by chejiangyi on 2017/4/7.
 */
public class CallBack {
    public interface Action0 {
        void invoke();
    }

    public interface Action1<T1> {
        void invoke(T1 t1);
    }
    public interface Action2<T1,T2> {
        void invoke(T1 t1,T2 t2);
    }

    public interface Func0<T0> {
        T0 invoke();
    }

    public interface Func1<T0,T1> {
        T0 invoke(T1 t1);
    }
    public interface Func2<T0,T1,T2> {
        T0 invoke(T1 t1,T2 t2);
    }
}
