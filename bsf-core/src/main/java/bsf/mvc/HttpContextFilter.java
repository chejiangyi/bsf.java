package bsf.mvc;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by chejiangyi on 2017/4/19.
 */
public class HttpContextFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpContext.init((HttpServletRequest) request, (HttpServletResponse) response);

        chain.doFilter(request, response);

        HttpContext.clear();

    }

    @Override
    public void destroy() {
    }
}
