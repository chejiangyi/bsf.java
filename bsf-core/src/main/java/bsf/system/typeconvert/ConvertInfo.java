package bsf.system.typeconvert;

/**
 * Created by chejiangyi on 2016/3/21.
 * 类型转换信息定义
 */
public class ConvertInfo
{
    public Class<?> fromType;
    public Class<?> toType;


    @Override
    public int hashCode() {
        return (fromType.getName()+" - "+toType.getName()).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return this.hashCode()==obj.hashCode();
    }
}