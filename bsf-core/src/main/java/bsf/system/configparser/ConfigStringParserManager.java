package bsf.system.configparser;

import java.util.HashMap;

/**
 * Created by chejiangyi on 2017/3/30.
 */
public class ConfigStringParserManager
{
    private static HashMap<Class, Object> connectPool = new HashMap();
    private static Object poolLock = new Object();

    public final <T> ConfigStringParser<T> GetParser(Class<T> cls)
{
    if (connectPool.containsKey(cls))
    {
        return (ConfigStringParser<T>)connectPool.get(cls);
    }
    else
    {
        synchronized (poolLock)
        {
            if (connectPool.containsKey(cls))
            {
                return (ConfigStringParser<T>)connectPool.get(cls);
            }
            ConfigStringParser<T> t = new ConfigStringParser<T>();
            connectPool.put(cls, t);
            return (ConfigStringParser<T>)connectPool.get(cls);
        }
    }

}
}