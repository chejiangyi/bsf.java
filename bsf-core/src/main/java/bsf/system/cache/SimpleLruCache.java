package bsf.system.cache;

/**
 * Created by chejiangyi on 2017/3/30.
 */

import bsf.base.BsfException;
import bsf.log.ErrorLog;

import java.util.*;

/**
 最近最久未使用算法（自己实现）
 算法:超过缓存长度时，根据读次数，最少读的缓存回收之（未清理的缓存，则清零读的次数）

 <typeparam name="T"></typeparam>
 <typeparam name="D"></typeparam>
 */
public class SimpleLruCache<T, D>
{
    private HashMap<T, LRUData<D>> cache = new HashMap();
    private Object lock = new Object();
    private int maxLength = 500;
    private float recoverPerLength = 0.5f; //回收长度百分百（折半回收）

    public SimpleLruCache(int maxlength)
    {
        maxLength = maxlength;
        if (maxLength < 5)
        {
            throw new BsfException("LRU缓存长度不建议小于"+5,this.getClass());
        }
    }

    private void tryRecovery()
    {
        if (cache.size() >= maxLength)
        {
            synchronized (lock)
            {
                if (cache.size() >= maxLength)
                {
                    //读次数列表
                    ArrayList<Integer> readcounts = new ArrayList<Integer>();
                    for (Map.Entry<T,LRUData<D>> c : cache.entrySet())
                    {
                        readcounts.add(c.getValue().ReadCount);
                    }
                    //先排序 获取只读列表中间值
                    int removereadcount = 0;
                    if (readcounts.size() >= 2)
                    {
                        Collections.sort(readcounts);
                        removereadcount = readcounts.get((int)(readcounts.size() * recoverPerLength));
                    }
                    //清理
                    Iterator it=cache.keySet().iterator();
                    while(it.hasNext())
                    {
                        Map.Entry<T,LRUData<D>> c=(Map.Entry<T,LRUData<D>>)it.next();
                        if (c.getValue().ReadCount <= removereadcount)
                        {
                            cache.remove(c.getKey()); //回收缓存
                            if (c.getValue().Data instanceof AutoCloseable)
                            {
                                try
                                {
                                    if(c.getValue().Data !=null)
                                    {
                                        ((AutoCloseable)c.getValue().Data).close();
                                    }
                                }
                                catch (Exception exp)
                                {
                                    ErrorLog.write("回收缓存时,AutoCloseable释放资源出错", exp, this.getClass());
                                }
                            }
                            it.remove();
                        }
                        else
                        {
                            c.getValue().ReadCount=0; //读次数清零
                        }

                    }
                }
            }
        }
    }

    public D get(T key)
    {
        try
        {
            if(cache.containsKey(key)) {
                LRUData<D> data = cache.get(key);
                data.ReadCount=data.ReadCount+1; //读次数仅要求相对准确即可
                return data.Data;
            }
        }
        catch (Exception e)
        {
            //高并发下会出错，不做处理
        }
        return null;
    }

    public D set(T key, D value)
    {
        tryRecovery();
        synchronized (lock)
        {
            if (!cache.containsKey(key))
            {
                LRUData<D> t = new LRUData<D>();
                t.Data=value;
                t.ReadCount=0;
                cache.put(key, t);
            }
            else
            {
                cache.get(key).Data=value;
            }
            return cache.get(key).Data;
        }
    }

    private static class LRUData<D>
    {
        /**
         数据
         */
        public D Data;

        /**
         访问读次数
         */
        public int ReadCount;

    }
}
