package bsf.http.pool;

import bsf.base.BsfConfig;
import bsf.system.configparser.ConfigStringParser;
import bsf.system.configparser.ConfigStringParserManager;

/**
 * Created by chejiangyi on 2017/3/31.
 * 默认的连接池管理,未涉及到多个连接池实现和多连接池（分片）管理实现，自动回收检查等。
 */
public class HttpClientPoolManager {
    private static HttpClientPool pool = null;
    private static HttpClientConfig config = null;
    private static Object lock = new Object();

    public static HttpClientPool GetPool() {
        HttpClientConfig confignow = new ConfigStringParserManager().GetParser(HttpClientConfig.class).Parse(BsfConfig.getDefaultHttpPoolConnectString(), HttpClientConfig.class);
        if (pool != null && config == confignow) {
            return pool;
        } else {
            synchronized (lock) {
                if (pool != null) {
                    pool.close();
                }
                pool = new HttpClientPool(confignow);
                return pool;
            }
        }
    }
}
