package bsf.http.protocol;

/**
 * Created by chejiangyi on 2017/3/31.
 */

/**
 服务器端返回通信协议

 */
public class ServiceResult
{
    /**
     返回值 1成功 -1失败 <0表示错误码 >0表示成功码

     */
    public int code;
    /**
     消息返回

     */
    public String msg = "";
    /**
     接受对象

     */
    public Object data;
    /**
     如列表，列表总数

     */
    public int total;

    /**
     服务器时间 （UTCNow - 1970-01-01）
     使用 System.currentTimeMillis() 暂时并非UtcNow
     */
    public long servertime = System.currentTimeMillis();
}
