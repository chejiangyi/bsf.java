package bsf.util;

import bsf.base.BsfException;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Properties;

/**
 * Created by Administrator on 2016/3/4.
 */
public class PropertiesUtil {
    public static String tryfindPropertiesPath(String fileName,Class cls) {
        ArrayList<String> filepaths = new ArrayList<String>();
        String temppath = fileName;
        if (!filepaths.contains(temppath))
            filepaths.add(temppath);
        Class[] clss = new Class[]{cls, PropertiesUtil.class};
        for (Class c : clss) {
            if (c != null) {
                temppath = StringUtils.stripEnd(FileUtil.getDirectoryPath(c), File.separator) + File.separator + fileName;
                if (!filepaths.contains(temppath))
                    filepaths.add(temppath);
                 URL uri = c.getClassLoader().getResource(fileName);
                if(uri!=null) {
                    temppath = uri.getPath();
                    if (!filepaths.contains(temppath))
                        filepaths.add(temppath);
                }
                uri = c.getResource(fileName);
                if(uri!=null)
                    temppath = uri.getPath();
                if (!filepaths.contains(temppath))
                    filepaths.add(temppath);
            }
        }
        for (String filepath : filepaths) {
            if (FileUtil.fileExsit(filepath))
                return filepath;
        }
        return null;
    }

    //根据key读取value
    public static String read( String filePath, String key) {
        try {
            InputStream in = null;
            try {
                Properties props = new Properties();
                in = new BufferedInputStream(new FileInputStream(filePath));
                props.load(in);
                String value = props.getProperty(key);
                return value;
            } finally {
                if (in != null)
                    in.close();
            }
        } catch (IOException exp) {
            throw new BsfException("PropertiesUtil-read", exp);
        }
    }


    //写入properties信息
    public static void write(String filePath, String parameterName, String parameterValue) {
        try {
            InputStream fis = null;
            OutputStream fos = null;
            try {
                Properties prop = new Properties();
                fis = new FileInputStream(filePath);
                //从输入流中读取属性列表（键和元素对）
                prop.load(fis);
                //调用 Hashtable 的方法 put。使用 getProperty 方法提供并行性。
                //强制要求为属性的键和值使用字符串。返回值是 Hashtable 调用 put 的结果。
                fos = new FileOutputStream(filePath);
                prop.setProperty(parameterName, parameterValue);
                //以适合使用 load 方法加载到 Properties 表中的格式，
                //将此 Properties 表中的属性列表（键和元素对）写入输出流
                prop.store(fos, "Update '" + parameterName + "' value");
            } finally {
                if (fis != null)
                    fis.close();
                if (fos != null)
                    fos.close();

            }
        } catch (IOException exp) {
            throw new BsfException("PropertiesUtil-write", exp);
        }
    }

}
