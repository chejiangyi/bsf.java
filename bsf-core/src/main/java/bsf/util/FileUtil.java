package bsf.util;

import bsf.base.BsfException;
import com.sun.tools.internal.ws.processor.util.DirectoryUtil;
import org.apache.commons.io.FileExistsException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.io.*;

/**
 * Created by chejiangyi on 2017/3/29.
 */
public class FileUtil {
    public static boolean fileExsit(String filepath) {
        File file = new File(filepath);
        return file.exists();
    }

    public static String getDirectoryPath(String path) {
        File file = new File(path);
        return file.getAbsolutePath();
    }

    public static String getDirectoryPath(Class cls) {
        File file = getJarFile(cls);
        if (file == null)
            return null;
        if(!file.isDirectory())
            file=file.getParentFile();
        return file.getAbsolutePath();
    }

    public static File getJarFile(Class cls)
    {
        String path = cls.getProtectionDomain().getCodeSource().getLocation().getFile();
        try
        {
            path = java.net.URLDecoder.decode(path, "UTF-8"); // 转换处理中文及空格
        }
        catch (java.io.UnsupportedEncodingException e)
        {
            return null;
        }
        return new File(path);
    }

    public static String getFilePath(String... paths) {
        StringBuffer sb = new StringBuffer();
        for (String path : paths) {
            sb.append(StringUtils.strip(path, File.separator));
            sb.append(File.separator);
        }
        return StringUtils.strip(sb.toString(), File.separator);
    }

    public static void createDirectory(String path) {
        File file = new File(path);
        if(!file.isDirectory())
            file=file.getParentFile();
        //如果文件夹不存在则创建
        if (!file.exists()) {
            file.mkdirs();
        } else {
        }
    }

    public static void appendAllText(String path, String contents) {
        try {
            FileWriter fw = null;
            PrintWriter pw = null;
            try {
                //如果文件存在，则追加内容；如果文件不存在，则创建文件
                File f = new File(path);
                fw = new FileWriter(f, true);
                pw = new PrintWriter(fw);
                pw.println(contents);
                pw.flush();
                fw.flush();
            } finally {
                if (pw != null)
                    pw.close();
                if (fw != null)
                    fw.close();
            }
        } catch (IOException exp) {
            throw new BsfException("FileUtil-appendAllText", exp);
        }

    }

    public static void writeAllText(String path, String contents) {
        try {
            File f = new File(path);
            if (f.exists()) {
                f.delete();
            } else {
            }
            //f.mkdirs();
            f.createNewFile();//不存在则创建
            BufferedWriter output = null;
            try {
                output = new BufferedWriter(new FileWriter(f));
                output.write(contents);
            } finally {
                if (output != null)
                    output.close();
            }
        } catch (IOException exp) {
            throw new BsfException("FileUtil-writeAllText", exp);
        }
    }

    public static String readAllText(String path) {
        try {
            File f = new File(path);
            if (f.exists()) {
                Long filelength = f.length();     //获取文件长度
                byte[] filecontent = new byte[filelength.intValue()];
                FileInputStream in = null;
                try {
                    in = new FileInputStream(f);
                    in.read(filecontent);
                } finally {
                    if (in != null)
                        in.close();
                }
                return new String(filecontent);//返回文件内容,默认编码
            } else {
                throw new FileNotFoundException(path);
            }
        } catch (IOException exp) {
            throw new BsfException("FileUtil-readAllText", exp);
        }
    }

    public static String lineSeparator() {
        return System.getProperty("line.separator");
    }
}
