package bsf.db.driver.sqlserver;

import bsf.db.base.DbConfigBase;
import bsf.db.base.DbConnBase;
import com.alibaba.druid.util.StringUtils;

/**
 * Created by chejiangyi on 2017/3/30.
 */
public class DbConfigSqlServerWithDruid {
    public String DriverClassName;
    public String Url;
    public int InitialSize;
    public int MaxActive;
    public int MinIdle;
    public boolean TestWhileIdle =false;
    public String Password = "";
    public String UserName="";
    /*兼容C#的Sqlserver写法*/
    public String InitialCatalog;//映射->数据库
    public String UserID;//映射->UserName

    @Override
    public String toString() {
        return "DbConfigSqlServerWithDruid{" +
                "DriverClassName='" + DriverClassName + '\'' +
                ", Url='" + Url + '\'' +
                ", InitialSize=" + InitialSize +
                ", MaxActive=" + MaxActive +
                ", MinIdle=" + MinIdle +
                ", TestWhileIdle=" + TestWhileIdle +
                ", Password='" + Password + '\'' +
                ", UserName='" + UserName + '\'' +
                ", InitialCatalog='" + InitialCatalog + '\'' +
                ", UserID='" + UserID + '\'' +
                '}';
    }

    public void init(DbConfigBase configBase)
    {
        //字符串兼容处理
        if(StringUtils.isEmpty(configBase.DataBase))
            configBase.DataBase = InitialCatalog;
        if(StringUtils.isEmpty(configBase.UserName))
            configBase.UserName = UserID;
        //默认端口号
        if(!configBase.Server.contains(":"))
            configBase.Server+=":1433";
        UserName = configBase.UserName;
        Password = configBase.Password;
        if(StringUtils.isEmpty(DriverClassName))
            DriverClassName = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
        if(StringUtils.isEmpty(Url))
            Url="jdbc:sqlserver://"+ configBase.Server+";DatabaseName="+configBase.DataBase;
        if(InitialSize == 0)
            InitialSize = configBase.MinPoolSize;
        if(MaxActive == 0)
            MaxActive = configBase.MaxPoolSzie;
        if(MinIdle ==0)
            MinIdle = configBase.MinPoolSize;
    }
}
