package bsf.db.base;

/**
 * Created by chejiangyi on 2017/3/30.
 */
public  class DbConfigBase {
    public String Type = "SqlServer";
    public String PoolType = "Druid";
    public String Server="";
    public String DataBase = "";
    public String UserName="";
    public String Password="";
    public int MaxPoolSzie=10;
    public int MinPoolSize=1;

    @Override
    public String toString() {
        return "DbConfigBase{" +
                "Type='" + Type + '\'' +
                ", PoolType='" + PoolType + '\'' +
                ", Server='" + Server + '\'' +
                ", DataBase='" + DataBase + '\'' +
                ", UserName='" + UserName + '\'' +
                ", Password='" + Password + '\'' +
                ", MaxPoolSzie=" + MaxPoolSzie +
                ", MinPoolSize=" + MinPoolSize +
                '}';
    }
}
