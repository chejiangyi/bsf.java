package bsf.db.base;

/**
 * Created by chejiangyi on 2016/3/23.
 */

import bsf.db.base.ParameterDirection;

/** 存储过程参数
 */
public class ProcedureParameter
{
    /** 参数方向
     */
    public ParameterDirection direction= ParameterDirection.INPUT;
    /** 参数值
     */
    public Object value;
}