package bsf.db.base;

import bsf.db.base.DbException;
import bsf.log.ErrorLog;


import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by chejiangyi on 2016/3/25.
 */
public class DbHelper {
//    public static  <T> List<T> toList(Class<T> cls, ResultSet rs) {
//        AutoMapHelper map = new AutoMapHelper();
//        return map.ToList(rs,cls);
//    }
//
    public static List<Map<String, Object>> toMapList(ResultSet rs)  {
        try {
            List<Map<String, Object>> list = new ArrayList<>();
            if (rs != null && !rs.isClosed()) {
                ResultSetMetaData meta = rs.getMetaData();
                int colCount = meta.getColumnCount();
                while (rs.next()) {
                    Map<String, Object> map = new HashMap<String, Object>();
                    for (int i = 1; i <= colCount; i++) {
                        String key = meta.getColumnName(i);
                        Object value = rs.getObject(i);
                        map.put(key, value);
                    }
                    list.add(map);
                }
            }
            return list;
        }
        catch (Exception exp)
        {
            throw new DbException("toMapList",exp);
        }
    }
}
