package bsf.db.base;

import bsf.base.BsfException;

/**
 * Created by chejiangyi on 2016/3/23.
 */
public class DbException extends BsfException {
    public DbException(String message) {
        super(message);
    }

    public DbException(String message,Class cls) {
        super(message,cls);
    }

    public DbException(String message, Throwable cause) {
        super(message, cause);
    }

    public DbException(String message, Throwable cause,Class cls) {
        super(message, cause,cls);
    }
}
