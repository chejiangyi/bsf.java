package bsf.serialization.json.system;

/**
 * Created by chejiangyi on 2016/3/18.
 */
public interface JsonSerialize {
      String serialize(Object obj);
      <T> T deserialize(String json, Class<T> cls);
}
