package bsf.serialization.json;

import bsf.serialization.json.fastjson.FastJsonProvider;
import bsf.serialization.json.gson.GsonProvider;
import bsf.serialization.json.system.JsonSerialize;
import bsf.serialization.json.system.JsonTypeEnum;

/**
 * Created by chejiangyi on 2016/3/3.
 * json 对外统一使用的json规范及兼容多种json实现
 */
public class JsonProvider {
	public JsonTypeEnum JsonType = JsonTypeEnum.FASTJSON;
	public String serialize(Object obj) {
		try {
			JsonSerialize js = getJsonSerialize();
			return js.serialize(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public <T> T deserialize(String json, Class<T> cls) {
		try {
			JsonSerialize js = getJsonSerialize();
			return js.deserialize(json, cls);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private JsonSerialize getJsonSerialize()
	{
		JsonSerialize js = null;
		if(JsonType == JsonType.GSON)
		{
			js = new GsonProvider();
		}
		else if(JsonType == JsonType.FASTJSON)
		{
			js = new FastJsonProvider();
		}
		return js;
	}
}
