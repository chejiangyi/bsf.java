package bsf.serialization.json.gson;

import bsf.serialization.json.system.JsonSerialize;
import com.google.gson.Gson;

/**
 * Created by chejiangyi on 2016/3/18.
 *  gson 二次封装及简化使用
 */
public class GsonProvider implements JsonSerialize {
    public String serialize(Object obj) {
        try {
            Gson gson = new Gson();
            return gson.toJson(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T deserialize(String json, Class<T> cls) {
        try {
            Gson gson = new Gson();
            return gson.fromJson(json, cls);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
